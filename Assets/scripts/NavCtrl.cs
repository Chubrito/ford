﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NavCtrl : MonoBehaviour {

    public void LoadScene(string sceneLoad)
    {
        SceneManager.LoadScene(sceneLoad);
    }
}
