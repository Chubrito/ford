﻿using UnityEngine;
using System.Collections;

public class UpgradeManager : MonoBehaviour {

    public Click click;
    public UnityEngine.UI.Text itemInfo;
    public float Wood_cost;
    public float stone_cost;
    public int count = 0;
    public int clickPower;
    public string itemName;
    private float _newCost;
    private float _newCoststone;


    // Update is called once per frame
    void Update()
    {
        itemInfo.text = itemName + "\nWood Cost: " + Wood_cost + "  \nStone Cost: " + stone_cost + "\nPower: " + clickPower ;
    }

    public void PurchasedUpgrade()
    {
        if (click.wood >= Wood_cost && click.stone >= stone_cost)
        {
            click.wood -= Wood_cost;
            click.stone -= stone_cost;
            count++;
            click.woodperclick += (clickPower);
            Wood_cost = Mathf.Round(Wood_cost * 1.115f);
            _newCost = Mathf.Pow(Wood_cost, _newCost = Wood_cost);

            stone_cost = Mathf.Round(stone_cost * 1.115f);
            _newCoststone = Mathf.Pow(stone_cost, _newCoststone = stone_cost);

        }

    }
}
