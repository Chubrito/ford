﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {
    public UnityEngine.UI.Text IventoryDisplay;
    public UnityEngine.UI.Text perClick;


    public static Click i;

    



    //forest

    public int ItemForest = 0;

    public float sappling = 0.00f;
    public float sapplingperclick = .15f;

    public float wood = 0.00f;
    public float woodperclick = 0.3f;

    public float stick = 0.00f;
    public float stickperclick = .2f;

    //mine


    public int ItemCave = 0;

    public float stone = 0.00f;
    public float stoneperclick = .25f;

    public float ironOre = 0.00f;
    public float ironperclick = .2f;

    public float coal = 0.00f;
    public float coalperclick = .3f;

    public float goldore = 0.00f;
    public float goldperclick = 0.1f;



    void Awake()
    {
        if (!i)
        {
            i = this;
            //DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

    }

    // Use this for initialization
    void Start () {
	
	}
	

    void Update()
    {
        perClick.text = "Wood per Click: " + woodperclick + "        Stone per Click: " + stoneperclick;

        IventoryDisplay.text = "Wood: " + Mathf.Round(wood  * 1000.0f)/ 1000.0f + "\nStick: " + Mathf.Round(stick  * 1000.0f)/ 1000.0f + "\nSappling: " + Mathf.Round(sappling  * 1000.0f)/ 1000.0f + "\n\n\nStone: " + Mathf.Round(stone * 1000.0f) / 1000.0f + "\nCoal: " + Mathf.Round(coal * 1000.0f) / 1000.0f + "\nIron Ore: " + Mathf.Round(ironOre * 1000.0f) / 1000.0f + "\nGold Ore: " + Mathf.Round(goldore * 1000.0f) / 1000.0f;
    }

    public void ClickForest()
    {
        ItemForest = Random.Range(1, 10);
        switch (ItemForest)
        {

            case 1: wood += woodperclick; break;
            case 2: sappling += sapplingperclick; break;
            case 3: stick += stickperclick; break;

            default:
                wood += woodperclick;
                break;
        }
    }

    public void ClickCave()
    {
        ItemCave = Random.Range(1, 10);
        switch (ItemCave)
        {

            case 1: stone += stoneperclick; break;
            case 2: coal += coalperclick; break;
            case 3: ironOre += ironperclick; break;
            case 4: goldore += goldperclick; break;

            default:
                stone += stoneperclick;
                break;
        }
    }
}
